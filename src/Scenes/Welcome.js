import React, { Component} from 'react';
// import { connect } from 'react-redux';
import { TextField, RaisedButton } from 'material-ui';
import { Card, CardTitle, CardText, CardActions } from 'material-ui/Card';
// import { login } from '../actions';

export class Welcome extends Component{
 
    state={
  	username:''
  }


  handleUserNameChange(){
  	const value=document.getElementById('username').value;
  	this.setState({username:value})

  }

  handleAddNewUser(){
  	// console.log(this.state)
  	this.props.username(this.state);
  	this.setState({username:''})

  }

	render() {
		return (
			<div style={{textAlign:'center',height:'100% !important'}} >
				
		<Card >
            <CardTitle
              title="Welcome to ChatApp"
            />
            <CardText >
              To start chat, please choose your handle for the Chat room.
              <br/>
              <TextField
                id="username"
                hintText="Dory, Nemo, ..."
                floatingLabelText="Display Name"
                value={this.state.username}
                onChange={this.handleUserNameChange.bind(this)}
              />
            </CardText>
            <CardActions>
              <RaisedButton label="Start" primary={true} onClick={this.handleAddNewUser.bind(this)} />
            </CardActions>
          </Card>
        
			</div>
		);
	}
}